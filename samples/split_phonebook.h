/******************************************************************************

    (C) Nick Marley, 2001 -

    This software is distributed under the GNU Lesser General Public Licence.
    Please read and understand the comments at the top of vf_iface.h before use!

FILE
    $Workfile$
    $Revision: 1.2 $
    $Author: tilda $
         
ORIGINAL AUTHOR
    Nick Marley

DESCRIPTION
    Sample code for the vformat library.

REFERENCES
    (none)    

MODIFICATION HISTORY
 *  $Log: split_phonebook.h,v $
 *  Revision 1.2  2001/10/24 05:29:24  tilda
 *  Tidy up.
 *
 *  Revision 1.1.1.1  2001/10/16 05:49:57  tilda
 *  Initial Import to CVS.
 *
 *******************************************************************************/

#ifndef NORCSID
static const char split_phonebook_h_vss_id[] = "$Header: /cvsroot/vformat/build/samples/split_phonebook.h,v 1.2 2001/10/24 05:29:24 tilda Exp $";
#endif

/*=============================================================================*
 Public Includes
 *=============================================================================*/
/* None */

/*============================================================================*
 Public Defines
 *============================================================================*/
/* None */

/*============================================================================*
 Public Functions
 *============================================================================*/


/*----------------------------------------------------------------------------*
 * NAME
 *      split_phonebook()
 * 
 * DESCRIPTION
 *      Read in the indicated file which is assumed to be a list of VCARDs.
 *      The component cards are written out to individual files.
 *
 * RETURNS
 *      TRUE <=> read / written OK.
 *----------------------------------------------------------------------------*/

extern bool_t split_phonebook(
    const char *p_filename,
    const char *p_outdir
    );
